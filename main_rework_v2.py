# © 2018 Lukas W. (itsAllDigital)
# Rework for new design off main window
import appJar
import subprocess
import time
# import sys

from loader import the_loader
from loader import config_rw

# Enable debugging messages?
debug = True

# Is downloading?
current_downloading = False

# The global stop the download var
stop_down = False

# Automatically try to oauth with tumblr. If fails enable to configure the OAuth


def auto_oauth():
    print("Trying auto login to tumblr")

    # Set the label
    app.setLabel("status_display", "Checking saved OAuth...")

    # Lock the config button
    app.disableButton("button_oauth")

    # Set the check ico to loading
    app.setImage("status_img", the_loader.get_paths("icon_path")+"auth_icon_checking.gif")

    if the_loader.check_auth(config_rw.read_current("consumer_key", the_loader.get_paths("config_path")),
                                 config_rw.read_current("consumer_secret", the_loader.get_paths("config_path")),
                                 config_rw.read_current("token_key", the_loader.get_paths("config_path")),
                                 config_rw.read_current("token_secret", the_loader.get_paths("config_path"))):
        # Switch to finished ico
        app.setImage("status_img", the_loader.get_paths("icon_path")+"auth_icon_success.gif")

        app.setLabel("status_display", "OAuth init was successful. Download can be started")

        print("Oauth successful. Unlock download button")
        app.enableButton("button_download")
    else:
        print("Not possible. OAuth failed")
        app.setImage("status_img", the_loader.get_paths("icon_path")+"auth_icon_denied.gif")
        app.setLabel("status_display", "OAuth init has failed. Please check data above")
        app.enableButton("button_oauth")


def oauth_save():

    # Disable the check button and download
    app.disableButton("button_download")
    app.disableButton("button_oauth")

    if oauth_check():
        config_rw.write_current(app.getEntry("consumer_key_field"), app.getEntry("consumer_secret_field"),
                                app.getEntry("token_key_field"), app.getEntry("token_secret_field"),
                                the_loader.get_paths("config_path"))

        # Enable button for download
        app.enableButton("button_download")

        # Give message of success
        app.infoBox("Authentication successful", "Logged in.\nDownload can be started now")

        # Run auto-check again
        app.thread(auto_oauth)
    else:
        # Re-enable the oauth check button
        app.enableButton("button_oauth")

        app.errorBox("Authentication failed", "Unable to authenticate with given credentials.\n"
                                              "Check for any typos and try again")


def oauth_check():
    if the_loader.check_auth(app.getEntry("consumer_key_field"),
                             app.getEntry("consumer_secret_field"),
                             app.getEntry("token_key_field"),
                             app.getEntry("token_secret_field")):
        print("OAuth check in main succeeded.")
        return True
    else:
        print("OAuth check in main failed.")
        return False


def init_stop():
    var1 = stop_prog()
    return var1


def stop_prog():
    global stop_down
    if current_downloading:
        if app.yesNoBox("Exit TumblrSnap", "Before you can exit the download will be stopped?\n"
                                            "Stop download and exit application?"):
            stop_down = True
            app.thread(timed_stop)
    else:
        if app.yesNoBox("Exit TumblrSnap", "Are you sure you want to exit?"):
            print("Stopped here")
            return True


def timed_stop():
    time.sleep(5.0)
    app.stop(True)


def open_file_browser():
    # Login
    the_loader.tumblr_login(the_loader.get_paths("config_path"))
    subprocess.Popen(r'explorer '+the_loader.get_images_path(the_loader.get_username(), "with_user"))


def placeholder():
    print("placeholder")


def show_info():
    app.showSubWindow("sub_info")


def init_load_cfg():

    # Set debug printing
    print("Running filling")

    config_path = the_loader.get_paths("config_path")

    # Setting fields on startup
    if config_rw.exist_check_file(config_path):

        # Print out if passed check
        print("Passed check")

        # Set the fields
        app.setEntry("consumer_key_field", text=config_rw.read_current("consumer_key", config_path))
        app.setEntry("consumer_secret_field", text=config_rw.read_current("consumer_secret", config_path))
        app.setEntry("token_key_field", text=config_rw.read_current("token_key", config_path))
        app.setEntry("token_secret_field", text=config_rw.read_current("token_secret", config_path))


def init_set_text():
    app.setLabel("status_display", "")
    app.setLabel("download_display", "")

    # Set the button text
    app.setButton("button_oauth", "Check OAuth")
    app.setButton("button_download", "Start Download")
    app.setButton("button_folder", "Open Folder")


def init_download():
    # Check if the variable exists
    var_exist = "isDownload" in globals()

    if not var_exist:
        # Make the global var
        global isDownload

        # Set it to false (default)
        isDownload = False

        # Run the actual downloader
        app.disableButton("button_download")
        app.thread(down_tumblr)
    else:
        if not isDownload:
            # Var exists start download
            app.thread(down_tumblr)


def down_tumblr():
    # Set the variable for killing the download + the current download status
    global stop_down
    global current_downloading

    # Make another check?
    chk_login = the_loader.tumblr_login(the_loader.get_paths("config_path"))
    if chk_login:

        # Set current download true
        current_downloading = True

        # Get me the user and favorites count
        user = the_loader.get_username()
        fav_count = the_loader.get_user_fav_count()

        # Check for the download folder
        the_loader.chk_download_store(user, the_loader.get_images_path(user, "no_user"))

        # Counter to keep track of where we are
        my_counter = 0

        # Amounts the system can go full 20 at my_limit
        my_rounds_tot = the_loader.counter_run(fav_count / 20)

        # The current rounds taken
        my_rounds_cur = 0

        # The limit on how many images (Not more than 20)
        my_limit = 20

        # Set "stop down" to False, because we start here
        stop_down = False

        # Label text to show in download
        label_download = 1

        # Set an empty timestamp, since offset limits me to 1000 only
        after_timestamp = 0

        # Keep track value
        track_count_success = 0
        track_count_fail = 0

        # Set the status to downloading
        app.setLabel("status_display", text="Downloading...")

        # Set the img to downloading
        app.setImage("status_img", the_loader.get_paths("icon_path")+"appli_icon_download.gif")

        while my_counter <= fav_count and not stop_down and current_downloading:

            # Get my like list
            if after_timestamp == 0:
                # print("First load. Going with offset")
                like_list = the_loader.get_likes("offset", my_limit, my_counter)
            else:
                # print("Switched to after. Bye bye offset")
                like_list = the_loader.get_likes("before", my_limit, after_timestamp)

            # Counter für die limit downloads
            cur = 0

            true_len = len(like_list["liked_posts"]) - 1

            # Set the current possiton up by the true_len counter (Shit my calcs don't allow that dynamic)
            if the_loader.chk_current(my_rounds_cur, my_rounds_tot):
                my_counter = my_counter + 20
            else:
                my_counter = the_loader.fav_remain(fav_count, my_counter)

            while cur < true_len and not stop_down:

                # Debug lines
                if debug:
                    print("DEBUG on downloader_def:",
                          "\nfav_count amount: ", fav_count,
                          "\nMy_Counter position: ", my_counter,
                          "\nAfter_Timestamp: ", after_timestamp)
                    print("True_Len: ", true_len, "\n")

                # Get post type
                post_type = like_list["liked_posts"][cur]["type"]

                if debug:
                    print("Additional information:\n"
                          "-----------------------\n"
                          "The current post type is: "+post_type,
                          "\n-------------------------\n")
                    #with open("dump.me", "w") as f:
                        #f.write(str(like_list["liked_posts"][cur]))
                        #f.close()

                # When post type is photo
                if post_type == "photo":

                    # Check for multiple images within the same post

                    # See if it's actually more posts
                    multi = the_loader.check_multiple_ulr(likes=like_list, current=cur, type="photo")

                    # Would print if multiple images are found
                    # print(multi[0])

                    if multi[0]:

                        if debug:
                            print("Multi image post detected.\n"
                                  "Loading those all down ;D")

                        # Set a val to count with
                        v1 = 0

                        while v1 < multi[1]:

                            # Set the current URL
                            cur_url = the_loader.gen_url(likes=like_list, current=cur, counter=v1, type="photo")

                            # Split the url to get the file name to display
                            title_part = the_loader.gen_file_name(url=cur_url, part="part")

                            # Split the url to get the file name to use to download
                            title_full = the_loader.gen_file_name(url=cur_url, part="full")
                            print("\ntitle_full: ", title_full)

                            # Set the title for the gui
                            app.setLabel("download_display", title_part)

                            # Make int into str and display (Shorted that) and apply
                            # app.setLabel("bottom_text", "Downloading " +
                            #              str(label_download) + " of " + str(fav_count))

                            if debug:
                                print("Going to check img!\n"
                                      "Following data is supplied:\n"
                                      "---------------------------\n"
                                      "Title_Part: " + title_part, "\n",
                                      "Title_Full: " + title_full, "\n",
                                      "Current_URL: " + cur_url,
                                      "\n---------------------------")

                            # Check for exsisting file in folder
                            if the_loader.chk_img(user, title_full, cur_url):
                                if not the_loader.download_likes(cur_url, the_loader.get_images_path(user, "with_user"),
                                                                 title_full):
                                    app.errorBox("Download Error", "While trying to download an image the download function could'nt connect to the server."
                                                                   "\nSkipping. Also pray it's working now again. Else you'll see me more often ;)")
                            else:
                                print("Image with same title exists already. Skipping that ;D."
                                      "\nYou don't have to thank me\n")

                            # Update the v1
                            v1 = v1 + 1
                    else:
                        # Set the current URL
                        cur_url = the_loader.gen_url(likes=like_list, current=cur, counter=0, type="photo")

                        # Split the url to get the file name to display
                        title_part = the_loader.gen_file_name(url=cur_url, part="part")

                        # Split the url to get the file name to use to download
                        title_full = the_loader.gen_file_name(url=cur_url, part="full")

                        # Set the title for the gui
                        app.setLabel("download_display", title_part)

                        # Make int into str and display (Shorted that) and apply
                        # app.setLabel("bottom_text", "Downloading " + str(label_download) + " of " + str(fav_count))

                        if debug:
                            print("\nGoing to check img!\n"
                                  "Following data is supplied:\n"
                                  "---------------------------\n"
                                  "Title_Part: " + title_part, "\n",
                                  "Title_Full: " + title_full, "\n",
                                  "Current_URL: " + cur_url,
                                  "\n---------------------------")

                        # Check for existing file in folder
                        if the_loader.chk_img(user, title_full, cur_url):
                            if not the_loader.download_likes(cur_url, the_loader.get_images_path(user, "with_user"),
                                                             title_full):
                                app.errorBox("Download Error",
                                             "While trying to download an image the download function could'nt connect to the server."
                                             "\nSkipping. Also pray it's working now again. Else you'll see me more often ;)")
                                track_count_fail = track_count_fail + 1
                            else:
                                track_count_success = track_count_success + 1
                        else:
                            print("Image with same title exists already. Skipping that ;D."
                                  "\nYou don't have to thank me\n")

                # Here we download the posts that are a video
                elif post_type == "video":
                    # Set the current URL
                    cur_url = the_loader.gen_url(likes=like_list, current=cur, counter=0, type="video")

                    # Split the url to get the file name to display
                    title_part = the_loader.gen_file_name(url=cur_url, part="part")

                    # Split the url to get the file name to use to download
                    title_full = the_loader.gen_file_name(url=cur_url, part="full")

                    # Set the title for the gui
                    app.setLabel("download_display", title_part)

                    # Make int into str and display (Shorted that) and apply
                    # app.setLabel("bottom_text", "Downloading " + str(label_download) + " of " + str(fav_count))

                    if debug:
                        print("\nGoing to check video!\n"
                              "Following data is supplied:\n"
                              "---------------------------\n"
                              "Title_Part: " + title_part, "\n",
                              "Title_Full: " + title_full, "\n",
                              "Current_URL: " + cur_url,
                              "\n---------------------------")

                    # Check for existing file in folder
                    if the_loader.chk_img(user, title_full, cur_url):
                        if not the_loader.download_likes(cur_url, the_loader.get_images_path(user, "with_user"),
                                                         title_full):
                            app.errorBox("Download Error",
                                         "While trying to download an image the download function could'nt connect to the server."
                                         "\nSkipping. Also pray it's working now again. Else you'll see me more often ;)")
                            track_count_fail = track_count_fail + 1
                        else:
                            track_count_success = track_count_success + 1
                    else:
                        print("Video with same title exists already. Skipping that ;D."
                              "\nYou don't have to thank me\n")
                else:
                    # The post isn't marked as "photo" and will be skipped right here. Happy code digging
                    print("Skipped a non photo/video post:\n"
                          "-------------------------\n"
                          "The current type is:", post_type,
                          "\n")

                # Set the last timestamp of the post contacted
                after_timestamp = like_list["liked_posts"][cur]["liked_timestamp"]

                # Set the current pointer one up
                cur = cur + 1

                # Set the label one image counter up
                label_download = label_download + 1

            # See where we stand with the counter_cur
            if the_loader.chk_current(my_rounds_cur, my_rounds_tot):
                my_rounds_cur = my_rounds_cur + 1
            else:
                my_limit = the_loader.fav_remain(fav_count, my_counter)

            if debug:
                print("DEBUG:",
                      "\nfav_count amount: ", fav_count,
                      "\nMy_Counter position: ", my_counter,
                      "\nAfter_Timestamp: ", after_timestamp)
                print("True_Len: ", true_len, "\n")

        # What will happen when stop down is set before all is done and gone ;D
        if my_counter <= fav_count and stop_down:
            app.infoBox("Download aborted", "Download stopped by user before all images where downloaded")
        elif stop_down:
            print("Seems like you killed the download on the last image.\nCongrats")
        elif my_counter == fav_count:
            app.infoBox("Finished", "Your download has been finished successfully."
                                    "\nSuccessful images downloaded: " + str(track_count_success) +
                                    "\nImages failed to download: " + str(track_count_fail))
        app.enableButton("button_download")

        # Set icon
        app.setImage("status_img", the_loader.get_paths("icon_path")+"auth_icon_saved.gif")

        # Set text
        app.setLabel("status_display", "Download finished")

        # Set current download to false
        current_downloading = False

# This my GUI here. Please I've been working hard on it.
# And yes I know it could be a lot prettier than this right now. (Talking about the GUI not the code, although...)


def set_main():
    app.setTitle("TumblrSnap v1.3 Beta")
    app.setSize(380, 400)
    app.setResizable(canResize=False)
    if the_loader.check_os() == 1:
        app.setIcon(the_loader.get_paths("icon_path")+"app_icon.ico")
    else:
        print("Linux don't like my icon")
    app.setStopFunction(init_stop)

    # Unset sticky for the subwindows to do it themselves
    # app.setSticky("news")
    # app.setPadding(20, 20)
    app.setExpand("both")
    app.setFont(14)

    # Set the menu
    app.addMenu("About", func=show_info)

    # Set up "the tools"
    # set_tools()


def set_config():
    # Starting SubFrame here
    app.startLabelFrame("OAuth Config", row=0, column=0)

    # Set bot sides to sticky
    app.setSticky("ew")

    # Set padding
    app.setPadding(10, 0)

    # Add my stuff
    app.addLabel("Consumer Key:", row=0, column=0)
    app.addEntry("consumer_key_field", row=0, column=1)

    app.addLabel("Consumer Secret:", row=1, column=0)
    app.addEntry("consumer_secret_field", row=1, column=1)

    # Get some spacing
    app.addLabel(" ", row=2, column=0)

    app.addLabel("Token Key:", row=3, column=0)
    app.addEntry("token_key_field", row=3, column=1)

    app.addLabel("Token Secret:", row=4, column=0)
    app.addEntry("token_secret_field", row=4, column=1)

    # Set the default texts
    app.setEntryDefault("consumer_key_field", "")
    app.setEntryDefault("consumer_secret_field", "")
    app.setEntryDefault("token_key_field", "")
    app.setEntryDefault("token_secret_field", "")

    # Finalize the sub-window
    app.stopLabelFrame()


def set_status():
    # Start the Status Frame
    app.startLabelFrame("Status", row=1, column=0)

    # Set bot sides to sticky
    app.setSticky("ew")

    # Add a status img (GIF) to show movement
    app.addImage("status_img", the_loader.get_paths("icon_path")+"auth_icon_pending.gif")

    # Add in the rest
    app.addLabel("status_display", text=None, row=1, column=0)
    app.addHorizontalSeparator(row=2, column=0)
    app.addLabel("download_display", text=None, row=3, column=0)

    # Stop the frame
    app.stopLabelFrame()


def set_buttons():
    # Start the console frame
    app.startLabelFrame("Console", row=2, column=0)

    # Set bot sides to sticky
    app.setSticky("ew")

    # Add ma buttons
    app.addButtons(names=["button_oauth",
                          "button_download",
                          "button_folder"],
                   funcs=[oauth_save,
                          init_download,
                          open_file_browser])

    # Set them all right
    app.disableButton("button_download")

    # Stop the frame
    app.stopLabelFrame()


def set_sub_info():
    app.startSubWindow("sub_info", title="About", modal=True)
    app.setResizable(canResize=False)
    app.setSize(500, 120)
    app.setSticky("w")
    app.setFont(10)

    app.addImage("creator_icon", the_loader.get_paths("icon_path")+"user_allDigital.gif", 0, 0)
    app.addLabel("created_who")

    app.setLabel("created_who", "TumblrSnap v1.3 BETA - Copyright (C) 2018  Lukas W.\n"
                                "----------------------------------------------------------------\n"
                                "This program comes with ABSOLUTELY NO WARRANTY\n"
                                "This is free software, and you are welcome to redistribute it "
                                "under certain conditions.")

    # Stop SubWindow
    app.stopSubWindow()


# The main start function


def main():
    # Build the main program here
    set_main()

    # Add the config tab for oauth
    set_config()

    # Add the status field
    set_status()

    # Add all buttons
    set_buttons()

    # Add the sub menu
    set_sub_info()

    # Check the config file
    config_rw.init_check(the_loader.get_paths("config_path"))

    # Load in the date from the config
    init_load_cfg()

    # Set all text unset from its default data
    init_set_text()

    # Auto OAuth
    app.thread(auto_oauth)

    # When all is set and done
    app.go()


if __name__ == "__main__":
    app = appJar.gui()
    main()
