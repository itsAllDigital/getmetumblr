# © 2018 Lukas W. (itsAllDigital)

import os
import platform
import pytumblr
import urllib.request
import math
import requests
from loader import config_rw

# from typing import Any, Union

down_check = False

debug = False


def tumblr_login(path):
    global myTumb
    myTumb = pytumblr.TumblrRestClient(config_rw.read_current('consumer_key', path),
                                       config_rw.read_current('consumer_secret', path),
                                       config_rw.read_current('token_key', path),
                                       config_rw.read_current('token_secret', path))
    try:
        chk = myTumb.info()["user"]["name"]
        print(chk)
        return True
    except KeyError:
        return False


def check_auth(cons_key, cons_sec, tok_key, tok_sec):
    print("Trying login")
    log = pytumblr.TumblrRestClient(
        consumer_key=cons_key,
        consumer_secret=cons_sec,
        oauth_token=tok_key,
        oauth_secret=tok_sec
    )
    user = log.info()
    try:
        chk = user["user"]["name"]
        print("Seems success", chk)
        return True
    except KeyError:
        print("Unable to login")
        return False


def chk_download_store(username, path):
    # Check if image folder exists
    if os.path.isdir(path):
        if os.path.isdir(path + username):
            return True
        else:
            os.mkdir(path + username)
            return True
    else:
        os.mkdir(path)
        os.mkdir(path + username)
        return True


def kill_download():
    global down_check
    down_check = True


def get_user_fav_count():
    all_likes = myTumb.info()['user']['likes']

    return all_likes


def get_username():
    user = myTumb.info()['user']['name']

    return user


def get_likes(down_mode, my_limit, position):
    if down_mode == "before":
        likes = myTumb.likes(limit=my_limit, before=position)
    else:
        likes = myTumb.likes(limit=my_limit, offset=position)

    return likes


def download_likes(url, path, title):
    try:
        urllib.request.urlretrieve(url, path + title)
        return True
    except ConnectionAbortedError or ConnectionError or ConnectionRefusedError or ConnectionResetError:
        try:
            urllib.request.urlretrieve(url, path + title)
            return True
        except ConnectionAbortedError or ConnectionError or ConnectionRefusedError or ConnectionResetError:
            print("Download error.")
            return False


def check_multiple_ulr(likes, current, type):

    # Check for multiple images in one post and return that somehow to be worked with
    if type == "photo":
        list_len = len(likes["liked_posts"][current]["photos"])

        if list_len > 1:
            return True, list_len
        elif list_len <= 1:
            return False, 0
    elif type == "video":
        list_len = len(likes["liked_posts"][current]["video_url"])

        if list_len > 1:
            return True, list_len
        elif list_len <= 1:
            return False, 0


def chk_img(user, title_full, url):

    # Debug output for now
    # if debug:
        # print("Debug of chk_img:\n"
              # "\nuser:", user,
              # "\ntitle:", title_full,
              # "\nThe output my command is working with:\n",
              # os.path.isfile(get_images_path(user, "with_user") + title_full),
              # "\n"
              # "\nBuild me the path: ", get_images_path(user, "with_user") + title_full)

    # Check image existence, her if not
    print("\nChecking flat\n")

    # Load img_flat
    if not img_flat(user, title_full):
        print("Check flat passed. File there: "
              "Digging deeper\n")
        if not img_deep(user, title_full, url):
            print("Check deep passed: "
                  "File exists")
            return False
        else:
            print("Check deep passed: "
                  "File does not exist")
            return True
    else:
        print("Check flat passed: "
              "File does not exist")
        return True


def img_flat(user, title_full):
    img_file = get_images_path(user, "with_user") + title_full
    if not os.path.isfile(img_file):
        # The file is not existing
        if debug:
            print("Image not there")
        return True
    else:
        # File is there, digging deeper
        return False


def img_deep(user, title_full, url):

    if debug:
        print("Checking deep")

    # Open the stream to get the length
    res_get = int(requests.get(url, stream=True).headers['Content-Length'])

    if debug:
        print("The res_get:", res_get)
        print("res_get type:", type(res_get))

    img_path = get_images_path(user, "with_user")+title_full

    if debug:
        print("The img_path:", img_path)

    img_size = os.path.getsize(img_path)

    if debug:
        print("The img_size:", img_size)
        print("img_size type", type(img_size))

    # Use resGet Content length to check the file size
    if img_size == res_get:
        if debug:
            print("The file size is equal. Won't download")
        return False
    else:
        if debug:
            print("The file size is not equal. Downloading")
        return True


def gen_url(likes, current, counter, type):
    # Refactor likes here
    # Photos
    if type == "photo":
        return likes["liked_posts"][current]["photos"][counter]["original_size"]["url"]
    elif type == "video":
        print(likes["liked_posts"][current]["video_url"])
        return likes["liked_posts"][current]["video_url"]


def gen_file_name(url, part):

    if part == "part":
        # Split the url first to get to the essential part
        split_1 = url.split("/")

        # Get the length of it and reduce it by one to get the last entry number for the file name
        split_len = len(split_1) - 1

        # Remove file extension to just get the file name
        split_2 = split_1[split_len].split(".")
        img_title = split_2[0]

        return img_title
    if part == "full":
        # Split the url first to get to the essential part
        split_1 = url.split("/")

        # Get the length of it and reduce it by one to get the last entry number for the file name
        split_len = len(split_1) - 1

        # Debug
        if debug:
            print("\nsplit_1: ", split_1,
                  "\nsplit_len: ", split_len,
                  "\nsplit_1 /w len: ", split_1[split_len])

        return split_1[split_len]


def get_paths(typ):
    # Get the platform system
    sys_chk = platform.system()

    if sys_chk == "Windows":
        if typ == "icon_path":
            ico_path = ".\\icons\\"
            return ico_path
        if typ == "config_path":
            conf_path = ".\\config\\"
            return conf_path
        if typ == "image_path":
            return False
        else:
            print("Unknown path was requested. Returning nothing")
            return

    if sys_chk == "Linux" or sys_chk == "Unix":
        if typ == "icon_path":
            ico_path = "./icons/"
            return ico_path
        if typ == "config_path":
            conf_path = "./config/"
            return conf_path
        if typ == "image_path":
            print("Needs other function. Go away")
            return
        else:
            print("Unknown path was requested. Returning nothing")
            return


def get_images_path(user, part):
    sys_chk = platform.system()

    if sys_chk == "Windows":
        if part == "with_user":
            img_path = ".\\images\\"+user+"\\"
            return img_path
        if part == "no_user":
            img_path = ".\\images\\"
            return img_path
    elif sys_chk == "Linux" or sys_chk == "Unix":
        if part == "with_user":
            img_path = "./images/"+user+"/"
            return img_path
        elif part == "no_user":
            img_path = "./images/"
            return img_path
    else:
        print("Seems I missed an OS here then :/")


def counter_run(counts):
    fl_count = counts
    fl_cur = 0.0
    while fl_cur <= fl_count:
        fl_cur = fl_cur + 1.0
    return math.floor(fl_cur)


def chk_current(rnd_cur, rnd_full):
    if rnd_cur < rnd_full:
        return True
    else:
        return False


def fav_remain(favs, counter):
    counter = favs - counter
    if not counter < 0:
        return counter


def check_os():
    if platform.system() == "Windows":
        return 1
    elif platform.system() == "Linux":
        return 2