TumblrSnap (getMeTumblr)
========================
This is a small tool, I build mainly for downloading and saving all favs on Tumblr (Currently only scripted for Windows, will add Linux/Mac support later. Read [this](https://gitlab.com/itsAllDigital/getmetumblr#why-only-windows) if you wanna know why).

Currently it only supports image posts and will ignore any posts containing text.

**Importand Notice:** Your credentials will be stored in plain text. So anyone with access to your files can potentialy use them to post, edit, delete posts, mess around with your liked posts, so on. Continue if you're aware of the potential risks.

Oh and keep in mind that you won't be able to send more than 2000 requests within an hour and 5000 within one day. It's a limit set by Tumblr to redouce server load (I have no ability to bypass that). If you want to be able to have more requests per hour/day you'll have to ask Tumblr to lift the restrictions on your application (Which will probably won't happen :/).

## How to use

Since this program is writen in Python you'll need Python to make the file usable or you'll donwload the packed executable.

**Using the pre-build packed executable**

From [here](https://gitlab.com/itsAllDigital/getmetumblr/tags) you can download the TumblrSnap_X.X_Win.zip (The X.X will be the current Version number).

After the download has finished extract the ZIP and run the main_rework_2.exe. The GUI should start up and all four entry fields should be empty awaiting your credentials. Enter them and click the "Check OAuth" button.

If you don't have your credentials and are unsure how to get them, follow this link in [how to get your OAuth-Credentials](https://gitlab.com/itsAllDigital/getmetumblr#how-to-get-your-oauth-credentials).

Otherwhise you can now enter them into the empty fields and after let the program check if they are valid. Then you just click the "Start Download" button.

All images/videos will be saved into the images folder within your username.

**Using the python file directly**

If you want to use the python file to run the program you have to download the packed ZIP file from [here](https://gitlab.com/itsAllDigital/getmetumblr/tags) or download the repository as a ZIP. Then unpack it inside the folder you want to run the program from.

Python is a needed program to run .py files so you need to download and install said program from here: [LINK](https://www.python.org/downloads/)

**Notice:** You need to tick the "Add to PATH" as well or your command promt wont recognize the "python" command and you'll either will have to reinstall the program (to then tick the checkbox) or manually add Python to your PATH. 
When the installer shows that the setup was successful, click on the disable path length limit to avoid any potential problems.

After that start a command prompt and enter the follwoing command to get the missing depenencies because those are not included in Python itself.

```
pip install pytumblr appjar
```

To run the old rework you need to use this command:
```
python main_rework.py
```

To run the new rework_v2 you need to use this command:
```
python main_rework_v2.py
```

Alternatively you can also use the .bat file to run the program with. All you need to to there is to double-click it like a normal program.

The program will load up. If this is your first time loading the program you will have to setup your OAuth-Credentials by clicking "Config OAuth".


If you don't have your credentials and are unsure how to get them, follow this link in [how to get your OAuth-Credentials](https://gitlab.com/itsAllDigital/getmetumblr#how-to-get-your-oauth-credentials).

Otherwhise you can now enter them into the empty fields and after letting the program check if they are valid you can then click the "Start Download" button.

All images will be saved into the images folder under your username.

### How to get your OAuth-Credentials

The credentials you need to optain are:

* Consumer key

* Consumer secret

* Token key

* Token secret

**1 - Get your consumer key and secret**

Open this [LINK](https://www.tumblr.com/oauth/register) to create an application.

**Notice:** Only fill out the fields marked with a star. For the callback-URL just enter a random URL as it doesn't matter for you. An example would be: https://empyt.gurl

When everything worked out you'll be taken to your application page where you can see your consumer key directly next unter your application name you did enter.

One below you can click "Show consumer secret" to also show the consumer secret.

You can either enter them both directly into the program or write them down for now.

**2 - Get your token key and secret**

Open this [LINK](https://api.tumblr.com/console/calls/user/info)

You'll be asked to enter your consumer key and token. After you enter them both and click the button bellow to continue you'll be asked if you want to grant the application access. Confirm to continue.

You'll be taken to a page were you only need to click the top button labled "Show keys". Which in turn will show you your consumer key and secret as well as both the token key and secret.

Copy your token key and secret and either enter them or write them down.

**3 - Enter your credentials**

When you have all your credentials enter them into the fields given withen the program after you hit "Config OAuth". Press "Check" to let the program confirm that your entered credentials are correct.

If so it will automaticaly save them and close the config window. Afterwards it will unlock the "Start Download" button with which you now can start your download ;D

## Issues

 - <strike>Currently something I'm still unsure of will block my script from getting new new images. This is noticable by the filename remaining the same while the counter still is going up. I'm investigating on that one.</strike>

 - <strike>Pyinstaller makes the program currently unusable. Checking on how to fix that.</strike>

**Other than that you'll have to tell me**

## <strike>Why only Windows</strike>
<strike>Thing is that I have to tell Python its paths (where the config is and where to store the images). Windows uses backslashes for its paths (**example:** C:\\).</strike>

<strike>Linux based systems and MacOS(Built on Unix) use the forwardslash to identify their directories (**example:** /home/user).</strike>

<strike>Right now the progam simply doesn't has the knowlage how to handle Linux/MacOS's forwardslash. But as said above I'll add that, after I addressed the one issue bugging me.</strike>

## What was used

Python - It's the programming language (and a few of its own modules)

pytumblr - A module writen and maintained by Tumblr itself (So all credit to them)

appjar - The module that helped me make my GUI (Read more on their website [HERE](https://appjar.info))

cx_feeze - Module that was used to build the executable (Read more on their website [HERE](https://anthony-tuininga.github.io/cx_Freeze))

As well as two modules writen from me for this purpose (the_loader and config_rw).

## Who made this

© 2018 Lukas W. (itsAllDigital)

This project is licensed under MIT License - Read more [here](https://gitlab.com/itsAllDigital/getmetumblr/blob/master/LICENSE)